import React from "react";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import App from "./components/App";
import Login from "./components/Login";

const createRoutes = () => (
    <Router>
        {localStorage.getItem("token") ?
        <Route exact path="/" component={App} />
        :
        <Redirect to="/login"/>
        }
        <Route exact path="/app" component={App} />
        <Route exact path="/login" component={Login} />
    </Router>
);

export default createRoutes;