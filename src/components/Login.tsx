import React from "react";
import { GoogleLogin } from "react-google-login";
import { useHistory } from "react-router-dom";
import { Card } from "react-bootstrap";

import "./login.css";
import { CLIENT_ID, TEXT, COOKIE_POLICY } from "../constants";

export default function Login() {
  const history = useHistory();

  const responseGoogle = (response: any) => {
    if (response.accessToken) {
      JSON.stringify(localStorage.setItem("token", response.tokenId));
      history.push("/app");
    }
  };

  return (
    <div className="row login-container">
      <div className="col-lg-4 col-md-3 col-sm-2"></div>
      <div className="col-lg-4 col-md-6 col-sm-8">
        <Card>
          <Card.Title>
            {" "}
            <h2 className="h2-style">Click Button Below to login</h2>
          </Card.Title>{" "}
          <p className="text-style">
            Condimentum id venenatis a condimentum. Dictum sit amet justo donec
            enim diam vulputate ut.
          </p>
          <p className="text-style">
            Fringilla ut morbi tincidunt augue interdum velit. Tincidunt vitae
            semper quis lectus nulla at volutpat diam ut. Ipsum dolor sit amet
            consectetur adipiscing. Nulla pharetra diam sit amet nisl.
          </p>
          <Card.Body>
            <GoogleLogin
              clientId={CLIENT_ID}
              buttonText={TEXT}
              onSuccess={responseGoogle}
              onFailure={responseGoogle}
              cookiePolicy={COOKIE_POLICY}
              className="button"
            />
          </Card.Body>
        </Card>
      </div>
      <div className="col-lg-4 col-md-3 col-sm-2"></div>
    </div>
  );
}
