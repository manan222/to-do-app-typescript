import React from "react";
import "./header.css";
import { Dropdown } from "react-bootstrap";

export default function Header({ name, email, image, onLogout }: any) {
  console.log(typeof name);
  return (
    <div className="d-flex main">
      <div className="col-lg-4"></div>
      <div className="col-lg-4">
        <h2 className="head-style">
          Welcome{" "}
          <span className="name">
            <b>{name.toUpperCase()}</b>
          </span>
          To Do App
        </h2>
      </div>
      <div className="col-lg-4">
        <div className="col-lg-12 d-flex">
          <div className="col-lg-5"></div>
          <div className="col-lg-7 d-flex">
            <div className="email">
              <p>{email}</p>
            </div>
            <div>
              <Dropdown className="oultine">
                <Dropdown.Toggle>
                  <img
                    src={image}
                    alt="ali"
                    id="dropdown-basic"
                    className="image"
                  />
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item onClick={onLogout}>Logout</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
