import React from "react";
import { SingleTask } from "./SingleTask";
import RLDD from "react-list-drag-and-drop/lib/RLDD";
import "./tasklist.css";

export const TaskList: React.FC<any> = ({ tasks, handleDrag }: any) => {
  return (
    <div className="list-container">
      <RLDD
        items={tasks}
        itemRenderer={(item: any) => {
          return <SingleTask key={item.id} task={item} />;
        }}
        onChange={handleDrag}
      />
    </div>
  );
};
