import React, { FunctionComponent } from "react";
import styled from "styled-components";
import { Task } from "./../models/task";

const InputData = styled.input`
  padding: 20px;
  border: 1px solid #8f94ff;
  border-bottom-left-radius: 10px;
  border-top-left-radius: 10px;
  width: 42%;
  font-size: 18px;
`;

const ButtonSubmit = styled.button`
  padding: 24px;

  border: 1px solid #8f94ff;
  border-bottom-right-radius: 10px;
  border-top-right-radius: 10px;
  background-color: #8f94ff;
  color: white;
  input:focus {
    outline: none;
  }
`;

const ButtonClear = styled.button`
  background-color: #c00b0b !important;
  color: white;
  margin-left: 30px;
  border-radius: 15px;
  background: #73ad21;
  padding: 20px;
  width: 100px;
  font-size: 18px;
`;

interface Props {
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onAdd: (event: React.FormEvent<HTMLFormElement>) => void;
  onClear: () => void;
  tasks: Task[];
  task: Task;
}

export const NewTaskForm: FunctionComponent<Props> = ({
  onChange,
  onAdd,
  onClear,
  tasks,
  task,
}) => (
  <form onSubmit={onAdd}>
    <InputData onChange={onChange} value={task.name} />
    <ButtonSubmit type="submit">Add a task</ButtonSubmit>
    {tasks.length > 0 && (
      <ButtonClear id="clear" onClick={onClear}>
        Clear{" "}
      </ButtonClear>
    )}
  </form>
);
