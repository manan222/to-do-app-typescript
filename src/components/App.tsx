import React, { Component } from "react";
import { Task } from "./../models/task";
import { NewTaskForm } from "./NewTaskForm";
import { TaskList } from "./TaskList";
import Wrapper from "./wrappers/Wrapper";
import jwt_decode from "jwt-decode";
import Header from "./Header";

interface State {
  newTask: Task;
  tasks: Task[];
  email: string;
  image: string;
  fullName: string;
}

class App extends Component<{}, State> {
  state = {
    newTask: {
      id: 1,
      name: "",
    },
    tasks: [],
    email: "",
    image: "",
    fullName: "",
  };
  componentDidMount() {
    let token: any = localStorage.getItem("token");
    let decoded: any = jwt_decode(token);
    console.log(decoded);
    this.setState({ email: decoded.email });
    this.setState({ image: decoded.picture });
    this.setState({ fullName: decoded.name });
  }

  addTask = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    this.setState((previousState) => ({
      newTask: {
        id: previousState.newTask.id + 1,
        name: "",
      },
      tasks: this.state.newTask.name
        ? [...previousState.tasks, previousState.newTask]
        : [...previousState.tasks],
    }));
  };

  handleTaskChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      newTask: {
        ...this.state.newTask,
        name: event.target.value,
      },
    });
  };

  handleClearTasks = () => {
    this.setState({ tasks: [] });
  };
  logOut = () => {
    localStorage.removeItem("token");
    window.location.href = "/login";
  };

  handleDrag = (items: any) => {
    this.setState({ tasks: items });
  };

  render() {
    return (
      <Wrapper>
        <Header
          image={this.state.image}
          email={this.state.email}
          name={this.state.fullName}
          onLogout={this.logOut}
        />
        <div className="row">
          <div className="col-lg-3 col-md-2"></div>
          <div className="col-lg-6 col-md-8 col-sm-12">
            <NewTaskForm
              task={this.state.newTask}
              onAdd={this.addTask}
              onClear={this.handleClearTasks}
              onChange={this.handleTaskChange}
              tasks={this.state.tasks}
            />
            <TaskList tasks={this.state.tasks} handleDrag={this.handleDrag} />
          </div>

          <div className="col-lg-3 col-md-2"></div>
        </div>
      </Wrapper>
    );
  }
}
export default App;
